#ifndef CLOOG_PPL_H
#define CLOOG_PPL_H

#ifndef CLOOG_INT_GMP
#define CLOOG_INT_GMP
#endif

#include <cloog/cloog.h>
#include <cloog/matrix/constraintset.h>
#include <cloog/ppl/backend.h>
#include <cloog/ppl/domain.h>

#endif /* define _H */
